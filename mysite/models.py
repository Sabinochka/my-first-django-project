# coding=utf-8
from django.db.models import Model, CharField, FloatField


class Dish(Model):
    name = CharField(max_length=255, verbose_name=u"Название")
    price = FloatField(verbose_name="Цена")
    weight = FloatField(verbose_name="Вес")

    def __unicode__(self):
        return self.name