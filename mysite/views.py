from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from django.views.generic import TemplateView

from mysite.models import Dish


def index(request):
    return HttpResponse("Hello, world. You're at the poll index.")


class HomeView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = {
            'dishes': Dish.objects.all()
        }
        return context


def index(request):
    return HttpResponse("Hello, world. You're at the poll index.")


class Page1View(TemplateView):
    template_name = "page1.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['Page1', 'SABINA', 'MEERIM', 'ALISHER', 'PAVEL', 'ARGEN', 'DIMA', 'ANTON'],
            'title': "people",
        }
        return context


class Page2View(TemplateView):
    template_name = "page2.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['Page2', 'SABINA', 'MEERIM', 'ALISHER', 'PAVEL', 'ARGEN', 'DIMA', 'ANTON'],
            'title': "people",
        }
        return context


class Page3View(TemplateView):
    template_name = "page3.html"

    def get_context_data(self, **kwargs):
        context = {
            'names': ['Page3', 'SABINA', 'MEERIM', 'ALISHER', 'PAVEL', 'ARGEN', 'DIMA', 'ANTON'],
            'title': "people",
        }
        return context

def minds(request):
    return HttpResponse("Quotes of great minds)")

def home(request):
    return HttpResponse("Welcome to your home..=)")


