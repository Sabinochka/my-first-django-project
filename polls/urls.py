from django.conf.urls import url

from mysite import views
from polls.views import polls

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^123/$', polls)
]